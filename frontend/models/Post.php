<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for collection "post".
 *
 * @property \MongoDB\BSON\ObjectID|string $_id
 * @property mixed $name
 * @property mixed $text
 * @property mixed $description
 * @property mixed $autor
 * @property mixed $date
 * @property mixed $private
 */
class Post extends \yii\mongodb\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function collectionName()
    {
        return ['test', 'post'];
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
            '_id',
            'name',
            'text',
            'description',
            'autor',
            'date',
            'private',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'text', 'description', 'autor', 'date', 'private'], 'safe']
        ];
    }

    public static function findAll10()
    {
        $customers = Post::find()
            ->indexBy('name')
            ->limit(10)
            ->where(['private' => 'No'])
            ->all();
        if (!empty($customers)) {
            return $customers;
        }
        else{
            return ['ss'];
        }
    }

    public static function findMyAndPublic10()
    {

        $customers = Post::find()
            ->indexBy('name')
            ->limit(10)
            ->where(['private' => 'No'])
            ->OrWhere(['autor' => 'Post1'])
            ->all();
        if (!empty($customers)) {
            return $customers;
        }
        else{
            return ['ss'];
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            '_id' => 'ID',
            'name' => 'Name',
            'text' => 'Text',
            'description' => 'Description',
            'autor' => 'Autor',
            'date' => 'Date',
            'private' => 'Private',
        ];
    }
}
